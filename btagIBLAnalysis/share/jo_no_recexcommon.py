##############################################################################
##############################################################################
### MAIN SWITCHES

ONLYEssentialInfo =False   ## write minimal amount of info on the output file
ReduceInfo        =False    ## write minimal amount of info on the output file
DoMSV             =False   ## include variables for MSV tagger
doSMT             =True   ## include variables for SMT tagger
JetCollections = [
    ##"AntiKt10LCTopoJets"
    'AntiKt4EMTopoJets',
    # 'AntiKt2PV0TrackJets',
    # 'AntiKt4LCTopoJets',
    ]

import AthenaRootComps.ReadAthenaxAODHybrid
svcMgr.EventSelector.InputCollections = [
    '/afs/cern.ch/work/d/dguest/public/rachael/DAOD_FTAG2.ttbar_mc16.pool.root']

evtPrintoutInterval = vars().get('EVTPRINT', 5000)
svcMgr += CfgMgr.AthenaEventLoopMgr( EventPrintoutInterval=evtPrintoutInterval )

svcMgr += CfgMgr.THistSvc()
from btagIBLAnalysis.configHelpers import get_short_name
for jet in JetCollections:
    shortJetName=get_short_name(jet)
    svcMgr.THistSvc.Output += [
        shortJetName+" DATAFILE='flav_"+shortJetName+".root' OPT='RECREATE'"]

from AthenaCommon.AlgSequence import AlgSequence
algSeq = AlgSequence()

from btagIBLAnalysis.configHelpers import setupTools
setupTools(ToolSvc, CfgMgr)

algSeq += CfgMgr.BTagVertexAugmenter()

### Main Ntuple Dumper Algorithm
for JetCollection in JetCollections:
    shortJetName=get_short_name(JetCollection)
    alg = CfgMgr.btagIBLAnalysisAlg(
        "BTagDumpAlg_"+JetCollection,
        OutputLevel=DEBUG,
        Stream=shortJetName,
        InDetTrackSelectionTool   =ToolSvc.InDetTrackSelTool,
        CPTrackingLooseLabel = ToolSvc.CPTrackingLooseLabel,
        TrackVertexAssociationTool=ToolSvc.TightVertexAssocTool,
        TriggerDecisionTool = ToolSvc.TrigDecisionTool,
        JVTtool=ToolSvc.JVT,
    ) #DEBUG
    alg.JetCollectionName = JetCollection
    alg.svxCollections = {'jet_sv1_': 'SV1'}
    alg.doSMT = doSMT
    if "AntiKt2PV0Track" in JetCollection or "Truth" in JetCollection:
        alg.JetPtCut = 10.e3
        alg.CleanJets     = False
        alg.CalibrateJets = False
    elif "Track" in JetCollection:
        alg.JetPtCut = 7.e3
        alg.CleanJets     = False
        alg.CalibrateJets = False
    else:
        alg.JetPtCut = 20e3
    alg.doSMT     =doSMT
    alg.ReduceInfo=ReduceInfo
    alg.EssentialInfo=ONLYEssentialInfo
    alg.DoMSV     =DoMSV
    alg.Rel20     =True
    alg.JetCleaningTool.CutLevel= "LooseBad"
    alg.JetCleaningTool.DoUgly  = True
    algSeq += alg

    ###print JetCollection
    from btagIBLAnalysis.configHelpers import get_calibration_tool
    ToolSvc += get_calibration_tool(CfgMgr, JetCollection, isAF2=False)
    print alg
