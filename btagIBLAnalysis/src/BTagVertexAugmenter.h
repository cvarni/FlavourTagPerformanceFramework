#ifndef BTAGGING_VERTEX_AND_TRACKAUGMENTER_HH
#define BTAGGING_VERTEX_AND_TRACKAUGMENTER_HH

#include "AthenaBaseComps/AthAlgorithm.h"
#include "GaudiKernel/ToolHandle.h"

class BTagVertexAugmenter: public AthAlgorithm {
public:
  BTagVertexAugmenter(const std::string& name,
                      ISvcLocator* pSvcLocator );

  virtual StatusCode  initialize();
  virtual StatusCode  execute();
  virtual StatusCode  finalize();
private:
};

#endif
