
#include "GaudiKernel/DeclareFactoryEntries.h"

#include "../btagIBLAnalysisAlg.h"
#include "../BTagVertexAugmenter.h"
#include "../BTagTrackAugmenter.h"

DECLARE_ALGORITHM_FACTORY( btagIBLAnalysisAlg )
DECLARE_ALGORITHM_FACTORY(BTagVertexAugmenter)
DECLARE_ALGORITHM_FACTORY(BTagTrackAugmenter)

DECLARE_FACTORY_ENTRIES( btagIBLAnalysis )
{
  DECLARE_ALGORITHM( btagIBLAnalysisAlg );
  DECLARE_ALGORITHM( BTagTrackAugmenter );
}
