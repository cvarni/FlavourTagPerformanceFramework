#ifndef TRACK_BRANCHES_HH
#define TRACK_BRANCHES_HH

namespace xAOD {
  class IParticle;
}
class TTree;

#include <vector>
#include <string>

// branch buffers are stored as an external class to cut down on
// (re)compile time
struct TrackBranchBuffer;
struct BTagTrackAccessors;

class TrackBranches
{
public:
  typedef std::vector<const xAOD::IParticle*> PartVector;

  TrackBranches();
  ~TrackBranches();

  // disable copying and assignment
  TrackBranches& operator=(TrackBranches) = delete;
  TrackBranches(const TrackBranches&) = delete;

  void set_tree(TTree& output_tree, const std::string& prefix) const;
  void fill(const PartVector& constituents);
  void clear();
private:
  TrackBranchBuffer* m_branches;
  BTagTrackAccessors* m_acc;
  // short-circuit the branch filling if no tree is set
  mutable bool m_active;
};

#endif // TRACK_BRANCHES_HH
