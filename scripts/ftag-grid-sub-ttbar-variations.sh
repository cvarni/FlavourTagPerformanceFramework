#!/usr/bin/env bash

if [[ $- == *i* ]] ; then
    echo "Don't source me!" >&2
    return 1
else
    set -eu
fi

ZIPFILE=job.tar
if [[ -f $ZIPFILE ]] ; then
    rm $ZIPFILE
fi

SCRIPT_DIR=$(dirname $BASH_SOURCE)
JO=jobOptions_ipmp.py
DSS=(
    mc15_13TeV.410003.aMcAtNloHerwigppEvtGen_ttbar_nonallhad.merge.AOD.e4441_s2726_r7772_r7676
    mc15_13TeV.410004.PowhegHerwigppEvtGen_UEEE5_ttbar_hdamp172p5_nonallhad.merge.AOD.e3836_s2726_r7772_r7676
)

for DS in ${DSS[*]}; do
    ${SCRIPT_DIR}/ftag-grid-sub.sh -j $JO -d $DS -z job.tar -p 1
done



